from django.apps import AppConfig


class HelloRedhatAppConfig(AppConfig):
    name = 'hello_redhat_app'
