from django.shortcuts import render
from django.http import HttpResponse
 
def index(request):
    my_dict = {'insert_me':"Message from views.py"}
    return render(request, 'hello_redhat_app/index.html', context=my_dict)
